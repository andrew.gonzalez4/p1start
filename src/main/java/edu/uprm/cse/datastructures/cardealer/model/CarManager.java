//IMPORTS
package edu.uprm.cse.datastructures.cardealer.model;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;  

@Path("/cars")
public class CarManager {

	
	private CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance(); 
	//Get Method
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] allCars = new Car[cList.size()];
		for (int i = 0; i < cList.size(); i++) {
			allCars[i] = cList.get(i);
		}
		return allCars;
	} 
	//Get Method with the ID
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCustomer(@PathParam("id") long id){
		Car result = null;
		for(Car target:cList) {
			if(target.getCarId() == id) {
				result = target;
			}
		}
		//If the car with the ID was found return the car
		if(result != null) {
			return result;
		}
		//If not return Error 404
		else {
			throw new NotFoundException();
		}
	}      
	
	//Adding a car
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car obj){
		
		for(Car c: cList) {
			if(obj.getCarId() == c.getCarId()) {
				return Response.status(Response.Status.CONFLICT).build();
			}
		}
		
		if(obj.getCarPrice() < 0) {
			return  Response.status(Response.Status.BAD_REQUEST).build();
		}
	
		cList.add(obj);
		return Response.status(201).build();
	}
	//Updating a car
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(Car obj){
		for (Car car : cList) {
			//If after iterating the id matches with another car just remove 
			if(car.getCarId() == obj.getCarId()){
				cList.remove(car);
				cList.add(obj);
			}
		}
		return Response.status(200).build();
	}     
	//Delete
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		for (int i = 0; i < cList.size(); i++) {
			if(cList.get(i).getCarId() == id){
				//If the ID Matches just remove the car
				cList.remove(cList.get(i));
				return Response.status(Response.Status.OK).build();
			}
		} 
		//If you didn't found a car with the ID return not found.
		return Response.status(Response.Status.NOT_FOUND).build(); 

	}
}      


